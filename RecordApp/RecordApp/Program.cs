﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace RecordApp
{
    class Program
    {
        const string RecordFilename = "Record.json";

        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                //Console.WriteLine(Assembly.GetExecutingAssembly().Location);
                string exe_path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string record_full_path = Path.Combine(exe_path, RecordFilename);

                //读取
                FileInfosRecord records = null;
                if (!File.Exists(record_full_path))
                {
                    records = CreateDefaultRecordfile(record_full_path);
                }
                else
                {
                    string data = File.ReadAllText(record_full_path);
                    records = JsonConvert.DeserializeObject<FileInfosRecord>(data);
                }

                //更新
                string path = args[0];
                string filename = Path.GetFileNameWithoutExtension(path);
                FileInfoRecord record = new FileInfoRecord() { name = filename, fullpath = path };

                if (!IsContain(records.infos,record))
                {
                    records.infos.Add(record);
                }

                //写入
                string finalStr = JsonConvert.SerializeObject(records,Formatting.Indented);
                File.WriteAllText(record_full_path, finalStr);

            }
            //Console.Read();
        }

        static FileInfosRecord CreateDefaultRecordfile(string path)
        {
            FileInfosRecord records = new FileInfosRecord();
            string str = JsonConvert.SerializeObject(records);
            File.WriteAllText(path, str);
            return records;
        }

        static bool IsContain(List<FileInfoRecord> list, FileInfoRecord l) {
            if (list.Count == 0) return false;
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].name == l.name) return true;
            }
            return false;
        }
    }

    class FileInfosRecord
    {
        public List<FileInfoRecord> infos = new List<FileInfoRecord>();
    }

    class FileInfoRecord
    {
        public string name;
        public string fullpath;
    }
}
