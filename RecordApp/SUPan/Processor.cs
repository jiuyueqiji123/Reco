﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace SUPan
{
    class Processor
    {

        public static bool IsCurrentRunWhenBoot{
            get {
                object o = RegistryHelpers.GetRegistryValue(
                        RegistryHive.LocalMachine,
                        @"SOFTWARE\Microsoft\Windows\CurrentVersion\Run",
                        "kuaipan");
                return o != null && !string.IsNullOrEmpty(o.ToString());
            }
        }

        //设置开机启动
        public static void SetRunWhenBoot(bool value)
        {
            RegistryKey ms_run = RegistryHelpers.GetRegistryKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run",RegistryHive.LocalMachine);
            string path = value ? Assembly.GetExecutingAssembly().Location : "";
            ms_run.SetValue("kuaipan", path);
        }

        public static void AddContexItem() {
            //[HKEY_CLASSES_ROOT\Directory\shell\DirReco]
            //[HKEY_CLASSES_ROOT\Directory\shell\DirReco\command]
            string path_value = GetRecoAppFullpath();
            RegistryKey reg = RegistryHelpers.GetRegistryKey(@"Directory\shell\DirReco", RegistryHive.ClassesRoot);
            if (reg == null)
            {
                reg = RegistryHelpers.CreateSubKey(@"Directory\shell", @"DirReco", RegistryHive.ClassesRoot);
                reg.SetValue("", "DirReco");
                reg = RegistryHelpers.CreateSubKey(@"Directory\shell\DirReco", @"command", RegistryHive.ClassesRoot);
            }

            reg = RegistryHelpers.GetRegistryKey(@"Directory\shell\DirReco\command", RegistryHive.ClassesRoot);
            reg.SetValue("", path_value);

        }

        private static string GetRecoAppFullpath()
        {
            string currentDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string cmdExeName = "RecordApp.exe";
            string fullpath = Path.Combine(currentDir, cmdExeName);
            string path_value = "\"" + fullpath + "\"" + " \"%1\"";
            return path_value;
        }
    }

    public class RegistryHelpers
    {
        //注意：经测试，这里的路径必须使用双反斜杠 \\aa\\bb 或者 @"aa\bb",否则会报错

        public static RegistryKey GetRegistryKey(string keyPath,RegistryHive hive)
        {
            RegistryKey hiveRegistry
                = RegistryKey.OpenBaseKey(hive,
                                          Environment.Is64BitOperatingSystem
                                              ? RegistryView.Registry64
                                              : RegistryView.Registry32);

            return hiveRegistry.OpenSubKey(keyPath,true);
        }

        public static RegistryKey CreateSubKey(string parentPath,string subKeyName, RegistryHive hive) {
            RegistryKey Root
                = RegistryKey.OpenBaseKey(hive,
                                          Environment.Is64BitOperatingSystem
                                              ? RegistryView.Registry64
                                              : RegistryView.Registry32);
            RegistryKey parentKey = Root.OpenSubKey(parentPath,true);
            if (parentKey != null) {
                RegistryKey subkey = parentKey.CreateSubKey(subKeyName);
                return subkey;
            }
            return null;
        }

        public static object GetRegistryValue(RegistryHive hive,string keyPath, string keyName)
        {
            RegistryKey registry = GetRegistryKey(keyPath,hive);
            return registry.GetValue(keyName);
        }
    }

}
