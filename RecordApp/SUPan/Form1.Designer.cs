﻿namespace SUPan
{
    partial class Pan
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pan));
            this.RecoListBox = new System.Windows.Forms.ListBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.radioButtonBoot = new System.Windows.Forms.RadioButton();
            this.ListcontextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ListcontextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // RecoListBox
            // 
            this.RecoListBox.ContextMenuStrip = this.ListcontextMenuStrip;
            this.RecoListBox.FormattingEnabled = true;
            this.RecoListBox.ItemHeight = 12;
            this.RecoListBox.Location = new System.Drawing.Point(2, 33);
            this.RecoListBox.Margin = new System.Windows.Forms.Padding(0);
            this.RecoListBox.Name = "RecoListBox";
            this.RecoListBox.Size = new System.Drawing.Size(357, 304);
            this.RecoListBox.TabIndex = 0;
            this.RecoListBox.DoubleClick += new System.EventHandler(this.OnItemDoubleClick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(364, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // radioButtonBoot
            // 
            this.radioButtonBoot.AutoSize = true;
            this.radioButtonBoot.Location = new System.Drawing.Point(8, 8);
            this.radioButtonBoot.Name = "radioButtonBoot";
            this.radioButtonBoot.Size = new System.Drawing.Size(71, 16);
            this.radioButtonBoot.TabIndex = 2;
            this.radioButtonBoot.TabStop = true;
            this.radioButtonBoot.Text = "开机启动";
            this.radioButtonBoot.UseVisualStyleBackColor = true;
            this.radioButtonBoot.Click += new System.EventHandler(this.radioBtnClick);
            // 
            // ListcontextMenuStrip
            // 
            this.ListcontextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.删除ToolStripMenuItem});
            this.ListcontextMenuStrip.Name = "ListcontextMenuStrip";
            this.ListcontextMenuStrip.Size = new System.Drawing.Size(181, 48);
            this.ListcontextMenuStrip.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnListContexMenuClick);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            // 
            // Pan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(364, 338);
            this.ControlBox = false;
            this.Controls.Add(this.radioButtonBoot);
            this.Controls.Add(this.RecoListBox);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.Name = "Pan";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "SUPan";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ListcontextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox RecoListBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.RadioButton radioButtonBoot;
        private System.Windows.Forms.ContextMenuStrip ListcontextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
    }
}

