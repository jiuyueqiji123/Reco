﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SUPan
{
    public partial class Pan : Form
    {

        KeyboardHook k_hook;
        DataManager dataManager;
        string dirPrefix = "dir*__";
        string filePrefix = "file*__";

        bool isRunWithBoot;

        public Pan()
        {
            InitializeComponent();

            Processor.AddContexItem();

            //安装键盘钩子
            k_hook = new KeyboardHook();
            k_hook.KeyDownEvent += new KeyEventHandler(hook_KeyDown);//钩住键按下
            k_hook.Start();//安装键盘钩子

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DataInitial();
        }

        private void DataInitial()
        {
            dataManager = new DataManager();
            dataManager.Init();

            ListBoxInit();
            this.radioButtonBoot.Checked = Processor.IsCurrentRunWhenBoot;
            isRunWithBoot = this.radioButtonBoot.Checked;
        }

        private void ListBoxInit()
        {
            //显示
            this.RecoListBox.Items.Clear();
            foreach (var item in dataManager.Infos)
            {

                if (Directory.Exists(item.fullpath))
                {
                    this.RecoListBox.Items.Add(dirPrefix + item.name);
                }
                else
                {
                    this.RecoListBox.Items.Add(filePrefix + item.name);
                }
            }
        }

        private void hook_KeyDown(object sender, KeyEventArgs e)
        {
            //显示/隐藏（Alt + S）
            if (e.KeyValue == (int)Keys.S && (int)Control.ModifierKeys == (int)Keys.Alt)
            {
                //显示最前或者隐藏
                if (this.WindowState == FormWindowState.Minimized)
                {
                    DataInitial();
                    this.WindowState = FormWindowState.Normal;
                    this.BringToFront();
                }
                else {
                    this.WindowState = FormWindowState.Minimized;
                }
            }

        }

        private void OnItemDoubleClick(object sender, EventArgs e)
        {
            string itemname = RecoListBox.SelectedItem.ToString();
            if (itemname.StartsWith(dirPrefix)) {
                string fullpath = dataManager.Infos[RecoListBox.SelectedIndex].fullpath;
                this.openFileDialog.InitialDirectory = fullpath;
                this.openFileDialog.Multiselect = true;
                this.openFileDialog.ShowDialog();
            }
        }

        private void radioBtnClick(object sender, EventArgs e)
        {
            isRunWithBoot = !isRunWithBoot;
            radioButtonBoot.Checked = isRunWithBoot;
            Processor.SetRunWhenBoot(isRunWithBoot);
        }

        private void OnListContexMenuClick(object sender, MouseEventArgs e)
        {
            //删除
            int selectIndex = RecoListBox.SelectedIndex;
            if (selectIndex >= 0)
            {
                if (dataManager.DelRecoInfo(dataManager.Infos[selectIndex]))
                {
                    ListBoxInit();
                }
            }
        }
    }

}
